import {ScheduleTasks} from "../../support/PageObjectsPages/scheduleTasksPage"

describe("Schedule Tasks", ()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")

    })

    it(" Schedule Tasks Data", ()=>{

        let sheTasks = new ScheduleTasks()

        sheTasks.clickSystem()
        sheTasks.clickScheduleTasks()
        sheTasks.verifyScheduleTable()
        sheTasks.verifyScheduleTab()
        sheTasks.clickEditButton()
        sheTasks.entSecoudsRunPred(data.appData.SecondsRunPeriod)
        sheTasks.clickUpdateButton()
        sheTasks.clickScheduleLink()
        sheTasks.verifySchedultTasksPageTitle()
        sheTasks.verifyScheduleTasksMainPageTitle()
    })
})