import { HelpOptions } from "../../support/PageObjectsPages/helpOptions";

describe('Help Page validation', ()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password,{failOnStausCode: false})
            cy.log("Logged in to the application successfully")
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the applicatio successfully")
    })

    it("Help Tab Training page link validation",()=>{

        let helpOp = new HelpOptions()
        helpOp.clickHelp()
        helpOp.verifyTrainiglinks()
        

    })

})