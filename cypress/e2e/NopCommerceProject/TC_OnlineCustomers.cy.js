import {OnlineCustomers,} from "../../support/PageObjectsPages/onLineCustomersPage"
describe("Add Customer Role", ()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it("On Line Custoemr ",()=>{

        let OnlnCust = new OnlineCustomers()

        OnlnCust.clickcustomers()
        OnlnCust.clickOnlineCustomer()
        OnlnCust.verifyOnlineCustomerData()
        OnlnCust.clickOnlineCustlink()
        OnlnCust.verifyOnlineCustomertitle()
        OnlnCust.verifymainPageTitle()
        
    })

})