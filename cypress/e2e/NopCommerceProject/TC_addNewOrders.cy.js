
import { AddNewOrders, addNewOrders } from "../../support/PageObjectsPages/addNewOrders";

describe("Add New Orders", () =>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it("Add New Orders", ()=>{

        let orders = new addNewOrders()

        orders.clickSales()
        orders.clickOrder()
        orders.entStartDate(data.appData.stDate)
        orders.entEndDate(data.appData.enDate)
        orders.selWarehouse()
        orders.selVendor()
        orders.selBillingCountry()
        orders.selPayMethod()
        orders.VerifyOrderMenu()  
        orders.VerifyOrderTable()
        orders.clickView()
        orders.clickChangeStatusBtn()
        orders.selOrderStatus()
        orders.clickSave()
        orders.clickYesBtn()
        orders.verifyOrderStatus()
        orders.verifyBillingShipping()
        
    })
})