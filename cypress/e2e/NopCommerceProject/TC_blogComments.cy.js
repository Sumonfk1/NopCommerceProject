import {BlogComments} from "../../support/PageObjectsPages/blogCommentsPage"

describe("Add Blog comments", ()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it(" Blog comment added on the list", ()=>{

        let BgComt = new BlogComments

        BgComt.clickContMangemnt()
        BgComt.clickBlogComnt()
        BgComt.verifyBlogPost()
        BgComt.clickBlogLink()
        BgComt.verifyBlogPageTitle()
        BgComt.verifyBlogMainPageTitle()
    })

})