import Customers from "../../support/PageObjectsPages/customers";
var randomEmail = Math.random().toString(36).substring(2,15)+"@gmail.com"
//const randomDate = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
describe('Add New Customer Manu Customers', ()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password ,{failOnStausCode: false} )
            cy.log("Logged in to the application successfully")
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the applicatio successfully")
    })

        it("AddNewCustomers",()=>
        {
           
                let cus = new Customers()
                cy.log("Entre customers info")

                cus.clickCustomers()
                cus.clickBarCustomers()
                cus.clickAddNew()
                //cus.entEmail(data.appData.Aemail)
                cus.entEmail(randomEmail)
                cus.entPassword(data.appData.PasWod)
                cus.entFirstName(data.appData.SearchFName)
                cus.entLastName(data.appData.SearchLName)
                cus.selGend()
                cus.selDob(data.appData.DateOfBirth)
                cus.selSave()
                cus.VerifyCustomerList()
        })
    })
