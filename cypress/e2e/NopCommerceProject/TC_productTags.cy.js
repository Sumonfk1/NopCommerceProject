import ProudctTags from "../../support/PageObjectsPages/productTags";

describe("Peoduct Tags", ()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it("Peoduct tags table data", function()
    {
        let ProTag = new ProudctTags()
        cy.log("Product Review data")

        ProTag.clickCatalog()
        ProTag.clickProdTags()
        ProTag.VerifyProTags()
        ProTag.clickProductTagsLink()
        ProTag.verifyproductTagsTitle()
    })
})