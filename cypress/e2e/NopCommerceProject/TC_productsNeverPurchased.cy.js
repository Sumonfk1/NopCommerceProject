import { ProductNaverPurchased } from "../../support/PageObjectsPages/productNaverPurchased";

describe('List of Products Never purchased',()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")

    })
    it('Product Never purchased',()=>{

        let ProdNvrPurc = new ProductNaverPurchased()
        cy.log("Entred Products Neve purchased")

        ProdNvrPurc.clickReports()
        ProdNvrPurc.clickProdNvrPurch()
        ProdNvrPurc.selCategory()
        ProdNvrPurc.selManufacturer()
        ProdNvrPurc.selStore()
        ProdNvrPurc.selVendor()
        ProdNvrPurc.clickStartDate()
        ProdNvrPurc.clickEndDate()
        ProdNvrPurc.listOfProducts()
    })

})