
import { AddCustomerRole,  } from "../../support/PageObjectsPages/addCustomerRole";
describe("Add Customer Role", ()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it('Customer Role', ()=>{
        let CustomerRole = new AddCustomerRole()
        cy.log("Entred Customer Role")

        CustomerRole.clickcustomers()
        CustomerRole.clickCustomerRole()
        CustomerRole.clickaddNew()
        CustomerRole.entName(data.appData.enName)
        CustomerRole.verifyAcive()
        CustomerRole.clickPurProd()
        
    })
})