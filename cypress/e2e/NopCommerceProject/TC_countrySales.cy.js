import { CountrySales } from "../../support/PageObjectsPages/countrySales";

describe('Report for Most of the country',()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the application successfully")

    })

    it('Country Sales Report',()=>{

        let CountSales = new CountrySales()
        cy.log("Entred Country sales report info")

        CountSales.clickCountryReports()
        CountSales.clickCountryReports()
        CountSales.clickStrDate()
        CountSales.clickEndDate()
        CountSales.selOrderStatus()
        CountSales.selPaymentStatus()
        CountSales.selReportsList()
        CountSales.clickLinkReposts()
        CountSales.verifyTitleofPage()
        CountSales.verifyMainPageTitle()
    })
})