import AddNewDiscount from "../../support/PageObjectsPages/addNewDiscount"
//import DatePickerPage from "../../support/PageObjectsPages/DatePickerPage"

describe('Add New Discount Manu Promotions',()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){

            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
            cy.log("Logged in to the application")
    })
    
})
    after(function(){
        cy.logout()
        cy.log("Log out from the aplication")
})


    it('Bar  Discounts',function(){

            let dis = new AddNewDiscount()
            cy.log("Entred Discount info")
            //DatePickerPage.selectRandomDate()

            dis.clickPromotions()
            dis.clickDiscount()
            dis.clickAddNew()
            dis.clickonOff()
            dis.verifyActive()
            dis.entName(data.appData.name)
            // dis.selDiscountType()
            dis.selSDate(data.appData.stDate)
            dis.selEDate(data.appData.enDate)
            dis.selDiscountLimit()
            dis.clickSave()
                                                        
                                                    
        })
    })


