import {MessageTemplates} from "../../support/PageObjectsPages/messageTemplates"

describe("Message Template", ()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the application successfully")

    })

    it("Message Templates verification",()=>{

        let MegsTemplate = new MessageTemplates()
        MegsTemplate.clickContManagement()
        MegsTemplate.clickMesgTemplates()
        MegsTemplate.entTempSearch(data.appData.TemplatesName)
        MegsTemplate.selActive()
        MegsTemplate.clickSearch()
        MegsTemplate.verifyTempName()
        MegsTemplate.clickMesgTemplink()
        MegsTemplate.verifyMesgTempTitle()
        MegsTemplate.verifyMesgTempMainTitle()
    })

})