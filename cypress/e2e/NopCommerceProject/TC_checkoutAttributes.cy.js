import CheckoutAttributes from "../../support/PageObjectsPages/checkoutAttributes";

describe("Specification Attributes",()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
        })
    })

    after(function(){
         //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it("Add new Checkout Attributed", () =>{

        let CheckoutAttri = new CheckoutAttributes()
        cy.log('Enter new Checkout Attributed')

        CheckoutAttri.clickCatalog()
        CheckoutAttri.clickAttributes()
        CheckoutAttri.clickCheckoutAttri()
        CheckoutAttri.clickAddNew()
        CheckoutAttri.clikcOnOf()
        CheckoutAttri.clickAttributeInfo()
        CheckoutAttri.entCkAtName(data.ChkAttb.ChckoutAttName)
        CheckoutAttri.selControlType()
        CheckoutAttri.checkRequired()
        CheckoutAttri.entCheckAttDisOrd(data.ChkAttb.CheckoutAttDisOrd)
        CheckoutAttri.clickSave()

    })

})