import vendor from "../../support/PageObjectsPages/vendor"
var randomEmail = Math.random().toString(36).substring(2,15)+"@gmail.com"
describe('Vendor info',()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
            cy.log("Logged in to the application")
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application")
    })

    it('Enter Vendor info',function(){
        
            let ven = new vendor()
            cy.log("Vendor info entred")

            ven.clickCustomers()
            ven.clickbarVendor()
            ven.clickAddNew()
            ven.clickSOnOff()
            //ven.selOnOff()
            ven.entName(data.appData.name)
            ven.entDesc(data.appData.Description)
            //ven.entEmail(data.appData.Email)
            ven.entEmail(randomEmail)
            ven.uploadFile()
            ven.entFirstName(data.appData.firstname)
            ven.entLastName(data.appData.lastname)
            ven.entAEmail(randomEmail)
            ven.selCountry()
            ven.selState()
            ven.entCounty(data.appData.county)
            ven.entPhone(data.appData.PhoneNumber)
            ven.clicksave()
        })
    })
