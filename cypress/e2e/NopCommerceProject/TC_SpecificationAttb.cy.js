import SpecificationAttributes from "../../support/PageObjectsPages/specificationAttb";
const ramdomString = Math.random().toString(36)
describe("Specification Attributes",()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it("Add New Specification Attributes", ()=>{

        let SpecifiAttrib = new SpecificationAttributes()
        cy.log("Enter new Specification Attribute")

        SpecifiAttrib.clickCatalog()
        SpecifiAttrib.clickAttributes()
        SpecifiAttrib.clickSpecifiAttb()
        SpecifiAttrib.clickAddGroup()
        SpecifiAttrib.entName(ramdomString)
        SpecifiAttrib.entDisOrd(data.appData.AttributeOrder)
        SpecifiAttrib.clickSave()
        SpecifiAttrib.VerifySuccessMesg()
        SpecifiAttrib.VerifyTableData()
       
    })
})