import AddLog from "../../support/PageObjectsPages/addLog"

describe("Add Log Menu System",()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password ,{failOnStausCode: false} )
            cy.log("Logged in to the application successfully")
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the applicatio successfully")
})

        it('Bar Log', function(){

            let log = new AddLog()
            cy.log("Verify log inof")

            log.clickSystem()
            log.clickLog()
            log.VerifyTableData()
            log.verifyTab()
            log.clickLogLink()
            log.verifyLogPageTitle()
        })
})