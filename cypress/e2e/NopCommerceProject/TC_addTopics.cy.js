//import AddNewTopic from "../../support/PageObjectsPages/addNewTopic"

import { AddNewTopic, addNewTopics } from "../../support/PageObjectsPages/addNewTopic"
describe('Add Topics', ()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it('Add Topics', ()=>
    {
        let topic = new AddNewTopic()
        cy.log('Entred Add Topics info')

        topic.clickContentManagement()
        topic.clickTopics()
        topic.clickAddNew()
        topic.entTitle(data.appData.Title)
        topic.entBody(data.appData.CBody)
        topic.clickSave()
        topic.clickBackList()
        topic.VerifyTopicTable()


    })
})