import AddNewManufacture from "../../support/PageObjectsPages/addNewManufacturer"

describe('Add New Manufacturer', ()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
            cy.log("Logged in to the appliction succssfully")
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it('Add_Manufacturer', () =>
    {
        
            let AM = new AddNewManufacture()
            const file = "Book1.xlsx"
            cy.log("Entred Add new Manufacturer info")

            AM.clickCatalog()
            AM.clickManufact()
            AM.clickAddNew()
            AM.clickAdvanced()
            AM.entName(data.appData.MName)
            AM.entDesc(data.appData.MDescription)
            //AM.UploadFile()
            AM.clickSave()
    })
    })
