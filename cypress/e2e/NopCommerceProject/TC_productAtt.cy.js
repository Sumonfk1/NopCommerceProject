import ProductAttributes from "../../support/PageObjectsPages/productAttributes";

describe("Product Attributes",()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it('Product attributes',function(){

        let ProAtt = new ProductAttributes()
        cy.log("Product Attributes data")

        ProAtt.clickCatalog()
        ProAtt.clickAttributes()
        ProAtt.ClickProdAtt()
        ProAtt.VerifyProAtt()

    })
})