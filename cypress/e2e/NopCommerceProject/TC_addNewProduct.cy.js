import AddNewProduct from "../../support/PageObjectsPages/addNewProduct"

describe('Add New Products', () =>{

    before(function() {
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
            cy.log("Logged in to the application successfully")
        })
        
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it('New Products',()=>{

            let ANP = new AddNewProduct()

            ANP.clickCatalog()
            ANP.clickProducts()
            ANP.clickAddNew()
            ANP.entProName(data.appData.prodName)
            ANP.entShotDesc(data.appData.Desc)
            ANP.entFullDesc(data.appData.FullDescripton)
            ANP.entsku(data.appData.Sku)
            ANP.selVandor()
            ANP.entStDate(data.appData.StDate)
            ANP.entEdDate(data.appData.EdDate)
            ANP.clickSave()
            ANP.verifyMgs()
            ANP.VerifyProductData()
            

    

        })
       
    })
