import {CustomerByOrderTotal} from "../../support/PageObjectsPages/customerByOrderTotal"

describe("Customer By Order Total", ()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")

    })

    it('Customer By Order Total',()=>{

        let customerOdr = new CustomerByOrderTotal()

        cy.log("Verify Customer order table")

        customerOdr.clickCustomer()
        customerOdr.clickCustomerOrder()
        customerOdr.verifyCustomerTable()
        customerOdr.clickCustomerReportsLink()
        customerOdr.verifyCustomerPageTitle()
        customerOdr.verifyCustomerMainPageTitle()
    })
})