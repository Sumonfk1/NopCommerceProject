
import AddGiftCards from "../../support/PageObjectsPages/addGiftCards";

describe('Add Gift Card',()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it('Add Gift Card', function()
    {
        let GiftCards = new AddGiftCards()
        cy.log('Entred Gift Cards info')

        GiftCards.clickSales()
        GiftCards.clickGiftCards()
        GiftCards.clickAddNew()
        GiftCards.selGiftCardType()
        GiftCards.entInitialValue(data.appData.Value)
        GiftCards.verifyCardActivited()
        GiftCards.clickCouponCode()
        GiftCards.verifyCouponCodeNum()
        GiftCards.entName(data.appData.name)
        GiftCards.entMessage(data.appData.Message)
        GiftCards.clickSave()
    })
})