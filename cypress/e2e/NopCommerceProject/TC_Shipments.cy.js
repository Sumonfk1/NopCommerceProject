import Shipments from "../../support/PageObjectsPages/shipments";


describe('Add Shipments', ()=>
{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password ,{failOnStausCode: false} )
            cy.log("Logged in to the application successfully")
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the applicatio successfully")
    })

    it('Add New Shipments', ()=>{
            let ship = new Shipments()

            ship.clickSales()
            ship.clickShipments()
            ship.SelDate(data.appData.stDate)
            ship.EndDate(data.appData.enDate)
            ship.SelWareHouse()
            ship.SelCountry()
            ship.SelState()
            ship.VerifyShipManagTableData()
        })
    })
