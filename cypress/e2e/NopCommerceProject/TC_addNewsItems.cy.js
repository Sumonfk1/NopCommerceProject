import AddNewsItems from "../../support/PageObjectsPages/addNewsItems"
describe("Manu Content Management",()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password ,{failOnStausCode: false} )
            cy.log("Logged in to the application successfully")
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the applicatio successfully")
    })

    it('Bar News Items', function(){

        let NewItems = new AddNewsItems()
        cy.log('Entred News Items info')

        NewItems.clickContentManagement()
        NewItems.clickNewItems()
        NewItems.clickAddNew()
        NewItems.entTitle(data.appData.Title)
        NewItems.entShotDes(data.appData.ShortDesc)
        NewItems.entFullDes(data.appData.FullDescription)
        NewItems.verifyAllowComments()
        NewItems.entStartDate(data.appData.Sdate)
        NewItems.entEndDate(data.appData.Edate)
        NewItems.verifyPublished()
        NewItems.clickSave()
                                
    })
})