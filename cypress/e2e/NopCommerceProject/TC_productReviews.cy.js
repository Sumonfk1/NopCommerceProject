import ProductReviews from "../../support/PageObjectsPages/productReviews";

describe("Product Review",()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })


    it("Product reviews data", function(){
        
        let ProRev = new ProductReviews()
        cy.log("Product Review data")

        ProRev.clickCatalog()
        ProRev.clickProductReviews()
        ProRev.VerifyProductReviews()
    })
})