//import {AddNewCategory, addNewCategoryLocators} from "../../support/PagesElements/addNewCategoryLocators.json";
import AddNewCategory from "../../support/PageObjectsPages/addNewCategory";
describe('Add New Categories', ()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password,{failOnStausCode: false})
            cy.log("Logged in to the application successfully")
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the applicatio successfully")
    })

    it('Add_Categories',function(){

            let NewCategory = new AddNewCategory
            cy.log('Entred New Categories info')

            NewCategory.clickCatalog()
            NewCategory.clickCategories()
            NewCategory.clickAddNew()
            NewCategory.entName(data.appData.enName)
            NewCategory.entDescription(data.appData.Description)
            NewCategory.selPaCategory()
            NewCategory.clickSave()
            NewCategory.clickCateLink()
            NewCategory.verifyCatePageTitle()
            NewCategory.verifyCateMainPageTitle()
        })
    })
