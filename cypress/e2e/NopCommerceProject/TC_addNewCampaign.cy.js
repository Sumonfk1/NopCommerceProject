import AddNewCampaign from "../../support/PageObjectsPages/addNewCampaign";

describe('Manu Promations',()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password ,{failOnStausCode: false} )
            cy.log("Logged in to the application successfully")
        })
    })

    after(function(){
        cy.logout()
        cy.log("Logged out from the applicatio successfully")
    })

    it('Bar Campaigns',function(){
        let cam = new AddNewCampaign()
        cy.log("Entred Campaign info")

        cam.clickPromotions()
        cam.clickCampaigns()
        cam.clickAddNew()
        cam.entName(data.appData.cname)
        cam.entSubject(data.appData.Csubject)
        cam.entBody(data.appData.CBody)
        cam.entPlanDate(data.appData.CPlanDate)
        cam.selLimitedStore()
        cam.selLimitedCustomer()
        cam.clicksave()
        cam.verifyEmailCampaings()
        cam.verifyEmailCampTitle()
        cam.verifyMainEmailPageTitle()
        cam.clickEdit()
        cam.editCamName(data.appData.CampEditName)
        cam.selCustomerRole()
        cam.clickeditSave()
        cam.verifySuccessfulMegs()

    })
})


