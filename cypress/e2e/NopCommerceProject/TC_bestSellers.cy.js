import { BestSellers } from "../../support/PageObjectsPages/bestSellers";

describe("Best Sellers Reports", ()=>{
    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId, data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")

    })

    it("Best Sellers Reports",()=>{

        let BestSell = new BestSellers()

        BestSell.clickReports()
        BestSell.clickBestSellers()
        BestSell.verifyReportsTable()
        BestSell.clickReportLink()
        BestSell.verifyBestSellersPageTitle()
        BestSell.verifyBestSellersMainTtitle()
    })

})