import AddAffiliates from "../../support/PageObjectsPages/addAffiliates"
//import {AddAffiliates, addAffiliatesLocators} from "../../support/PageObjectsPages/addAffiliates"
describe('Add Affiliates',()=>{

    before(function(){
        cy.fixture('nopcommerce').then(function(data){
            globalThis.data=data
            cy.login(data.login.emailId,data.login.password)
        })
    })

    after(function(){
        //cy.logout()
        cy.log("Logged out from the application successfully")
    })

    it('Should allow user to add a new Affiliates', () =>{

        let Affits = new AddAffiliates()
        cy.log('Entred Affiliates info')

        //AA.fillOutAddAffiliates()

        Affits.clickPromotions()
        Affits.clickAffiliates()
        Affits.clickAddNew()
        Affits.entFirstName(data.appData.firstname)
        Affits.entLastName(data.appData.lastname)

        
    })
})