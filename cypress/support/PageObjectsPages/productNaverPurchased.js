const productNeverPurchasedLocators = require ('../PagesElements/productNeverPurch.json')
    let brokenLinks = 0
    let activeLinks = 0
export class ProductNaverPurchased
{
    //"Menu Reports Tar"
    clickReports(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.report).click({force: true})}
    //"Bar ProductNaverPurchased Tab"
    clickProdNvrPurch(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.prodNvrPurched).click({force: true})}
    //"Selct a Category "
    selCategory(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.category).select('Books')
        .each(($el,index,$list)=>{
            cy.log($el.text())
        })}
    //"Select Manufacturer"
    selManufacturer(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.manufacturer).select('Nike')
        .each(($el,index,$list)=>{
            cy.log($el.text())
        })}
    //"Select Store"
    selStore(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.store).select('Test store 2')
        .each(($el, index, $list)=>{
            cy.log($el.text())
        })}
    //"Selct Vendors"
    selVendor(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.Vendor).select('Vendor 2')
        .each(($el,index,$list)=>{
            cy.log($el.text())
        })}
    //"Start Date"
    clickStartDate(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.startDate).click()
        cy.get("a[aria-label='Previous']").click({force: true})
        cy.get('#StartDate_dateview table tbody tr td').each(($el,index,$list)=>{
            var date = $el.text()
            if(date == '13')
            {
                cy.wrap($el).click({force: true})
            }
        })}
    //"End Date"
    clickEndDate(){cy.xpath(productNeverPurchasedLocators.ProductNeverPurchased.endDate).click()
        cy.get("a[aria-label='Next']").eq(1).click({force: true})
            cy.xpath('/html/body/div[5]/div/div/div[1]/a[2]').click({force: true})
                cy.get('.k-calendar-view table tbody tr td').each(($el,index,$list)=>{
                    cy.log($el.text())}).contains('May').click({force: true})
        cy.get('#EndDate_dateview table tbody tr td').each(($el, index, $list)=>{
            var eDate = $el.text()
            if(eDate == '29')
            {
                cy.wrap($el).click({force: true})
            }
        })}
    //"Name Of the Product reports never purchased"
    listOfProducts(){cy.get('#neversoldreport-grid tbody tr td').each(($el,index,$list)=>{
        cy.log($el.text())
    })}
}