const BestSellersLocators = require('../PagesElements/bestSellers.json')

    let brokenLinks = 0
    let activeLinks = 0
export class BestSellers
{
    //"Menu Reports Tab"
    clickReports(){cy.xpath(BestSellersLocators.BestSellers.report).click({force: true})}
    //"BestSellers Bar Tab"
    clickBestSellers(){cy.xpath(BestSellersLocators.BestSellers.besrSellers).click({force: true})}
    //"Verify Reports Table Data"
    verifyReportsTable(){cy.get(BestSellersLocators.BestSellers.reportTable)
        .should('contains.text','Apple iCam').each(($row, index, $rows) =>{
            cy.log($row.text())
    })}
    //" BestSellers Report Link"
    clickReportLink(){cy.xpath(BestSellersLocators.BestSellers.reportLink).invoke('removeAttr','target').click()}

    //"verify BestSellers Reports page Title and also find out all the active links and broken links of the page"
    verifyBestSellersPageTitle(){cy.get(BestSellersLocators.BestSellers.reportsTitle)
        .should('contains.text','Reports')
        cy.get('a').each(($link, index)=>{
            const href = $link.attr('href')
            if(href){
                cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`*****link ${index +1} is Broken Link *****${href}`)
                        brokenLinks++
                        
                    }else
                    {
                        cy.log(`***** link ${index + 1} Active Link *****`)
                        activeLinks++
                    }
                })
            }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
        }).go('back')}
        
    //"BestSellers Main Page title"
    verifyBestSellersMainTtitle(){cy.xpath(BestSellersLocators.BestSellers.reportsMainPageTitle)
        .should('have.text','\nBestsellers\n')}
}