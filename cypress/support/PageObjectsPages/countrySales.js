const countrySalesLocators = require ('../PagesElements/countrySales.json')
    let brokenLinks = 0
    let activeLinks = 0
export class CountrySales
{
    //"Menu Reports Tar"
    clickReports(){cy.xpath(countrySalesLocators.CountrySales.report).click({force: true})}
    //"Bar Country Sales"
    clickCountryReports(){cy.xpath(countrySalesLocators.CountrySales.countrysales).click({force: true})}
    //"Start Date"
    clickStrDate(){cy.xpath(countrySalesLocators.CountrySales.startDate).click()
            cy.get(countrySalesLocators.CountrySales.DayStart).each(($el, index, $list)=>{
                var date = $el.text()
                if(date == '10')
                {
                    cy.wrap($el).click({force: true})
                }
            })}
    //"End Date"
    clickEndDate(){cy.xpath(countrySalesLocators.CountrySales.endDate).click()
            cy.xpath(countrySalesLocators.CountrySales.yearArrow).click({force: true})
            cy.xpath(countrySalesLocators.CountrySales.years).click({force: true})
            cy.get(countrySalesLocators.CountrySales.months).each(($el, index,$list)=>{
                cy.log($el.text())}).contains("Jun").click({force:true})
            cy.get(countrySalesLocators.CountrySales.DayEnd).each(($el, indx,$list)=>{
                var eDate = $el.text()
                if(eDate == '30')
                {
                    cy.wrap($el).click({force:true})
                }
            })}
    //"Select Order Status"
    selOrderStatus(){cy.get(countrySalesLocators.CountrySales.orderStatus).select('Complete')
        .each(($el, index, $list)=>{
            cy.log($el.text())
    })}
    //" select Payment Status "
    selPaymentStatus(){cy.get(countrySalesLocators.CountrySales.paymentStatus).select('Paid')
        .each(($el, index, $list)=>{
            cy.log($el.text())
        })}
    //" List Country of Reports"
    selReportsList(){cy.get(countrySalesLocators.CountrySales.reportsList)
        .each(($row, index, $rows)=>{
            cy.log($row.text())

    })}
    //"Link of the Reports page"
    clickLinkReposts(){cy.xpath(countrySalesLocators.CountrySales.linkReports)
        .invoke('removeAttr','target').click()}
    //"Title of the Reports page"
    verifyTitleofPage(){cy.get(countrySalesLocators.CountrySales.titleReports)
        .should('have.text','Reports')
        cy.get('a').each(($link, index)=>{
            const href = $link.attr('href')
            if(href)
            {
                cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`*****link ${index +1} is Broken Link *****${href}`)
                        brokenLinks++
                    }else
                    {
                        cy.log(`***** link ${index + 1} Active Link *****`)
                        activeLinks++
                    }
                })
            }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
        }).go('back')}

        //"Verify Main Reports page Title"
    verifyMainPageTitle(){cy.xpath(countrySalesLocators.CountrySales.MainPageTitle)
        .contains('Country sales')}
}
