const addCustomerRoleLocators = require('../PagesElements/addCustomerRoles.json')
export class AddCustomerRole
{
    //"Menu customers tab"
    clickcustomers(){cy.xpath(addCustomerRoleLocators.CustomerRoles.customer).click({force: true}) 
        return}
    //"Bar Customer Roles"
    clickCustomerRole(){cy.xpath(addCustomerRoleLocators.CustomerRoles.customerroles).click({force: true}) 
        return}
    //"Add New button "
    clickaddNew(){cy.xpath(addCustomerRoleLocators.CustomerRoles.addNew).click({force: true} ) 
        return}
    //"Customer role Name"
    entName(en){cy.xpath(addCustomerRoleLocators.CustomerRoles.name).type(en) 
        return}
    //"Customer Role Active"
    verifyAcive(){cy.xpath(addCustomerRoleLocators.CustomerRoles.active).should('be.checked') 
        return}
    //"Purchased Product"
    clickPurProd(){cy.xpath(addCustomerRoleLocators.CustomerRoles.purchasedProd).click()
        return
    }
        
    }
        
       
    
