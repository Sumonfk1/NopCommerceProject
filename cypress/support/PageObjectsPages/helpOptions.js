const helpLocators = require('../PagesElements/help.json')

    let brokenLinks = 0
    let activeLinks = 0

   export class HelpOptions
    {
        //" Menu Help Tab"
        clickHelp(){cy.xpath(helpLocators.help.Help).click({force: true})}
        //" Verify all the links of Training page Tab"
        verifyTrainiglinks(){cy.xpath(helpLocators.help.Training)
            .invoke('removeAttr','target').click({force: true})
            cy.get('a').each(($link,index)=>{
               const href= $link.attr('href')
               if(href){
                cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`*****link ${index + 1} is Broken Link ****${href}`)
                        brokenLinks++
                    }
                    else
                    {
                        cy.log(`***** link ${index + 1} Active Link *****`)
                        activeLinks++
                    }
                })
               }
            }).then(($links)=>{
                const totalLinks = $links.length
                cy.log(`*****total links**** ${totalLinks}` )
                cy.log(`**** broken links ***** ${brokenLinks}`)
                cy.log(`***** active links***** ${activeLinks}`)
            }).go('back')
        }
        

    }
