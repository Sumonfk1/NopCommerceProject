class AddLog
{
    // Elements for AddLog

    system="//p[normalize-space()='System']"
    log="//p[normalize-space()='Log']"
    tab="#log-grid_wrapper tr"
    tableData = "//table[@id='log-grid']//tbody//tr"
    logLink="//a[normalize-space()='log']"
    logPageTitle = "//h1[@id='log']"



    //"Menu System" click option 
    clickSystem(){cy.xpath(this.system).click({force: true})}
    //"Bar Log" click option 
    clickLog(){cy.xpath(this.log).click({force: true})}
    //"Verify Table data"
    VerifyTableData(){cy.xpath(this.tableData)
        .should('contains.text','Error 404. The requested page (/admin/scheduletask/runtask) was not found')
            .and('have.length','15')
            .each(($row, index, $rows) =>{
                cy.log($row.text())
            })}
    //"Table Log" verify option 
    verifyTab(){cy.get(this.tab).eq(2)
        .within(function(){
            cy.get('td').eq(2).should('contains.text','Error 404. The requested page (/admin/scheduletask/runtask) was not found')
                           
                                
    })}
    //"Log link"
    clickLogLink(){cy.xpath(this.logLink).invoke('removeAttr','target').click()}
    //"Log Page Title"
    verifyLogPageTitle(){cy.xpath(this.logPageTitle).should('have.text','Log').go('back')}
 }                   

export default AddLog