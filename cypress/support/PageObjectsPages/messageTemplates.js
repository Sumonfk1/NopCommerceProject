const MessageTemplatesLocators = require('../PagesElements/messageTemplates.json')

    let brokenLinks=0
    let activeLinks=0
export class MessageTemplates
{
    //"Menu Content Management"
    clickContManagement(){cy.xpath(MessageTemplatesLocators.MessageTemplates.contentManagement).click({force: true})}
    //"Message Templates Bar Tab"
    clickMesgTemplates(){cy.xpath(MessageTemplatesLocators.MessageTemplates.mesgTemp).click({force: true})}
    //"Enter Search Keywords"
    entTempSearch(ts){cy.xpath(MessageTemplatesLocators.MessageTemplates.tempSearch).type(ts)}
    //"select Is Active"
    selActive(){cy.xpath(MessageTemplatesLocators.MessageTemplates.active).select('Active only').should('be.visible')}
    //"Search button"
    clickSearch(){cy.xpath(MessageTemplatesLocators.MessageTemplates.searchBtn).should('exist').click()}
    //"Verify Templates Name"
    verifyTempName(){cy.get(MessageTemplatesLocators.MessageTemplates.tempName)
        .should('contains.text','Blog.BlogComment').and('be.visible').each(($row,index,$rows)=>{
            cy.log($row.text())
        })}
    //"Message Template Link"
    clickMesgTemplink(){cy.xpath(MessageTemplatesLocators.MessageTemplates.mesgTempLink)
        .invoke('removeAttr','target').click()}
        
    //"Verify Message Template Ttile and also find out all the active links and broken links of the page"
    verifyMesgTempTitle(){cy.xpath(MessageTemplatesLocators.MessageTemplates.mesgTempTitle)
        .should('contains.text','Message templates')
        cy.get('a').each(($link, index)=>{
           const href = $link.attr('href')
           if(href){
                cy.request({url:href}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`***** link ${index +1} is Broken Link *****${href}`)
                        brokenLinks++
                    }else
                    {
                        cy.log(`***** link ${index + 1} Active Link******`)
                        activeLinks++
                    }
                })
           }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
        }).go('back')}

    //"Verify Message Template Main page title"
    verifyMesgTempMainTitle(){cy.xpath(MessageTemplatesLocators.MessageTemplates.mesgTempMainTitle)
        .should('contains.text','Message templates')}
}