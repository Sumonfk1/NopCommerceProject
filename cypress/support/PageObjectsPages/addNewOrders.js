const addNewOrdersLocators = require('../PagesElements/addNeworders.json')
export class addNewOrders
{
    //" Menu Sales"
    clickSales(){cy.xpath(addNewOrdersLocators.AddNewOrders.sales).click({force: true}) 
        return}
    //"Bar Order"
    clickOrder(){cy.xpath(addNewOrdersLocators.AddNewOrders.orders).click({force: true}) 
        return}
    //"Enter Start Date"
    entStartDate(sd){cy.xpath(addNewOrdersLocators.AddNewOrders.startDate).type(sd) 
        return}
    //Enter End Date
    entEndDate(ed){cy.xpath(addNewOrdersLocators.AddNewOrders.endDate).type(ed) 
        return}
    //"Select Warehouse"
    selWarehouse(){cy.xpath(addNewOrdersLocators.AddNewOrders.warehouse).select("Warehouse 1 (New York)")
        return}
    //"Select Vendor"
    selVendor(){cy.xpath(addNewOrdersLocators.AddNewOrders.vendor).select("Vendor 1") 
        return}
    //"Select Billing Country"
    selBillingCountry(){cy.get(addNewOrdersLocators.AddNewOrders.billingCountry).each(($billingCountry, index, list)=>{
        if($billingCountry.text() === 'United States'){
            cy.log("Country Selected")
            cy.wrap($billingCountry).click({force:true}).should('be.visible')
        }
        else
        {
            cy.log(" Current Country",$billingCountry.text())
        }
    })
        return}
    //"Select Payment Method"
    selPayMethod(){cy.xpath(addNewOrdersLocators.AddNewOrders.paymentMethod).select("Credit Card") 
        return}
    //"Verify Order Table Menu Data"
    VerifyOrderMenu(){cy.xpath(addNewOrdersLocators.AddNewOrders.ordersMenu).should("contains.text",'Order status')
        .each(($row, index, $rows)=>{
            cy.log($row.text())
        })}
    //"Verify Order Table Data "
    VerifyOrderTable(){cy.xpath(addNewOrdersLocators.AddNewOrders.ordersTable)
        .should("contains.text","Shipped").and('have.length','5').each(($row, index, $rows) =>{
            cy.log($row.text())
        })}
    //"Updated Order Status"
    clickView(){cy.xpath(addNewOrdersLocators.AddNewOrders.updateOrder).click() 
        return}
    //"Change Order Status"
    clickChangeStatusBtn(){cy.xpath(addNewOrdersLocators.AddNewOrders.changeStatus).click() 
        return}
    //"Order Status "
    selOrderStatus(){cy.xpath(addNewOrdersLocators.AddNewOrders.orderStatus)
        .select("Complete") 
        return}
    //" Order status change save"
    clickSave(){cy.xpath(addNewOrdersLocators.AddNewOrders.save).click() 
        return}
    //" Yes Button"
    clickYesBtn(){cy.xpath(addNewOrdersLocators.AddNewOrders.YesBtn).click() 
        return}
    //" Confirm Order Status"
    verifyOrderStatus(){cy.xpath(addNewOrdersLocators.AddNewOrders.confirmStatus)
        .should('have.text','Complete') 
        return}
    //"Billing and Shipping "
    verifyBillingShipping(){cy.xpath(addNewOrdersLocators.AddNewOrders.billingShipping)
        .should('contains.text','brenda_lindgren@nopCommerce.com').each(($row, index, $rows)=>{
            cy.log($row.text())
        })}
        
    }
    
    
    
    
