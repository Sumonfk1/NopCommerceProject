
    let brokenLinks = 0
    let activeLinks = 0
class ProudctTags
{
    catalog = "//p[normalize-space()='Catalog']"
    prodTags = "//p[normalize-space()='Product tags']"
    dataProTag = "//table[@id='product-tags-grid']//tbody//tr"
    prodTagsLink = "//a[normalize-space()='product tags']"
    productTagsTitle = "//h1[@id='product-tags']"

    //"Menu Calalog" click option     
    clickCatalog(){cy.xpath(this.catalog).click({force: true})}
    //"Bar ManuFactuires" click option 
    clickProdTags(){cy.xpath(this.prodTags).click({force: true})}
    //"Verify Product review tabel data"
    VerifyProTags(){cy.xpath(this.dataProTag).should('contains.text','computer').and('have.length','15')
        .each(($row, index, $rows) =>{
            cy.wrap($row).within(function(){
                cy.get('td').each(function($celData, index, $columns){
                    cy.log($celData.text())
        
                })
            })
        })}
    //"Product Tags link"
    clickProductTagsLink(){cy.xpath(this.prodTagsLink).invoke('removeAttr','target').click()}
    //"Verify Product Tags Page Title"
    verifyproductTagsTitle(){cy.xpath(this.productTagsTitle).should('contains.text','Product tags')
        cy.get('a').each(($link, index) =>{
            const href = $link.attr('href')
            if(href){
                cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`*****link ${index + 1} is Broken link *****${href}`)
                        brokenLinks++
                    }else
                    {
                        cy.log(`***** link ${index + 1} Active link *****`)
                        activeLinks++
                    }
                })
            }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
        }).go('back')
    
    }     
            
        
}

export default ProudctTags