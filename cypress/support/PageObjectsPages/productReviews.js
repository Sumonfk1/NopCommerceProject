    let brokenLinks =0
    let activeLinks = 0
class ProductReviews
    
{
    catalog = "//p[normalize-space()='Catalog']"
    prodRev = "//p[normalize-space()='Product reviews']"
    dataProdRev = "//table[@id='productreviews-grid']//tbody//tr"
    


    //"Menu Calalog" click option     
    clickCatalog(){cy.xpath(this.catalog).click({force: true})}
    //"Bar ManuFactures" click option 
    clickProductReviews(){cy.xpath(this.prodRev).click({force: true})}
    //"Verify product review table data"
    VerifyProductReviews(){cy.xpath(this.dataProdRev)
        /*.should('contains.text',"Some sample review")
        .each(($row, index, $rows) =>{
            cy.log($row.text())*/
            cy.get('a').each(($link, index)=>{
                const href = $link.attr('href')
                if(href){
                    cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                        if(response.status>=400){
                            cy.log(`*****link ${index + 1} is broken link *****${href}`)
                            brokenLinks++
                        }else
                        {
                            cy.log(`*****link ${index + 1} Active link *****`)
                            activeLinks++
                        }
                    })
                }
            }).then(($links)=>{
                const totalLinks = $links.length
                cy.log(`*****total links**** ${totalLinks}` )
                cy.log(`**** broken links ***** ${brokenLinks}`)
                cy.log(`***** active links***** ${activeLinks}`)
        })
        }}
    
    


export default ProductReviews