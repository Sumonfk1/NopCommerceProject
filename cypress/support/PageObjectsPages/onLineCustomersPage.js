const OnlineCustomersLocators = require('../PagesElements/listOfOnlineCustomers.json')

    let brokenLinks = 0
    let activeLinks = 0
export class OnlineCustomers
{
    //"Menu Customers Tab"
    clickcustomers(){cy.xpath(OnlineCustomersLocators.OnlineCustomers.customer).click({force: true})
        return
    }
    //"Bar Online Customer Tab"
    clickOnlineCustomer(){cy.xpath(OnlineCustomersLocators.OnlineCustomers.onLineCust).click({force: true}) 
        return
    }
    //"Verify Customers Info table data"
    verifyOnlineCustomerData(){cy.xpath(OnlineCustomersLocators.OnlineCustomers.customerinfo)
        .should('contains.text','United States').each(($row, index, $rows)=>{
            cy.log($row.text())
        })}
    //" Online Customers LinkPage"
    clickOnlineCustlink(){cy.xpath(OnlineCustomersLocators.OnlineCustomers.linkOnLineCust)
        .invoke('removeAttr','target').click()
        return
    }
    //"Verify On Line Custmers titile"
    verifyOnlineCustomertitle(){cy.xpath(OnlineCustomersLocators.OnlineCustomers.onlinecustomers)
        .should('contains.text','Online customers')
        cy.get('a').each(($link, index)=>{
            const href = $link.attr('href')
            if(href){
                cy.request({url:href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`*****link ${index + 1} is Broken link ****${href}`)
                        brokenLinks++
                    }else
                    {
                        cy.log(`******link ${index + 1} Active link ******`)
                        activeLinks++
                    }
                })
            }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
        })
        .go('back')
        return
        
    }
    //"Verify Main Page Online Customer title"
    verifymainPageTitle(){cy.xpath(OnlineCustomersLocators.OnlineCustomers.mainPageOnlinePage)
        .should('contains.text','Online customers') 
        cy.get('a').each(($link, index)=>{
            const href = $link.attr('href')
            if(href){
                cy.request({url:href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`******link ${index + 1} is Broken link *******${href}`)
                        brokenLinks++
                    }else
                    {
                        cy.log(`******link ${index + 1} Active link ******`)
                        activeLinks++
                    }
                })
            }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
        })
        return
    }

   
    
}