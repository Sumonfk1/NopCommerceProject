class ProductAttributes
{
    catalog = "//p[normalize-space()='Catalog']"
    attributes = "//p[normalize-space()='Attributes']"
    dataProductAtt = "//table[@id='products-grid']//tbody//tr"
    proAttbriutes = "//p[normalize-space()='Product attributes']"

    //"Menu Calalog" click option     
    clickCatalog(){cy.xpath(this.catalog).click({force: true})}
    //"Bar ManuFactures" click option 
    clickAttributes(){cy.xpath(this.attributes).click({force: true})}
    //"Bar Meanufactures" click option
    ClickProdAtt(){cy.xpath(this.proAttbriutes).click({force: true})}
    //"Verify Product attributes table data"
    VerifyProAtt(){cy.xpath(this.dataProductAtt).should('contains.text','Custom Text')
    .and('have.length','9')
        .each(($row, index, $rows) =>{
            cy.log($row.text())
        })}
}
export default ProductAttributes