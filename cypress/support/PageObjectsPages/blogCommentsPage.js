const BlogCommentsLocators = require('../PagesElements/blogComments.json')

    let brokenLinks = 0
    let activeLinks = 0
export class BlogComments
{
    //"Content Management Menu"
    clickContMangemnt(){cy.xpath(BlogCommentsLocators.BlogComments.contentManagement).click({force: true})}
    //"Blog Comments Bar"
    clickBlogComnt(){cy.xpath(BlogCommentsLocators.BlogComments.blogComnt).click({force: true})}
    //"Blog post table list"
    verifyBlogPost(){cy.xpath(BlogCommentsLocators.BlogComments.blogList)
        .should('contains.text','How a blog can help your growing e-Commerce business')
        .each(($row, index, $rows)=>{
            cy.log($row.text())
        })}
    //"Blog Link"
    clickBlogLink(){cy.xpath(BlogCommentsLocators.BlogComments.blogLink)
        .invoke('removeAttr','target').click()}
    //"Blog Page Title and also find out all the active links and broken links of the page"
    verifyBlogPageTitle(){cy.xpath(BlogCommentsLocators.BlogComments.blogPageTitle)
        .should('have.text','Blog')
        cy.get('a').each(($link, index)=>{
           const href =  $link.attr('href')
           if(href){
            cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                if(response.status>=400){
                    cy.log(`*****link ${index + 1} is Broken link *****${href}`)
                    brokenLinks++
                }else
                {
                    cy.log(`***** link ${index + 1} Active link ******`)
                    activeLinks++
                }
            })
           }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)

        }).go('back')}
    //"Blog Main Page Title"
    verifyBlogMainPageTitle(){cy.xpath(BlogCommentsLocators.BlogComments.blogMainPageTitle)
        .should('contains.text','Blog comments')}
}