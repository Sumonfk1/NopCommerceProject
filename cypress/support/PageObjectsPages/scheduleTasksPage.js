const ScheduleTasksLocators = require('../PagesElements/scheduleTasks.json')

export class ScheduleTasks
{
    //"Menu System Tab"
    clickSystem(){cy.xpath(ScheduleTasksLocators.ScheduleTasks.system).click({force: true})}
    //"Schedule Tasks Bar"
    clickScheduleTasks(){cy.xpath(ScheduleTasksLocators.ScheduleTasks.scheduleTasks).click({force: true})}
    //"Verify Schedule Tasks Table Data"
    verifyScheduleTable(){cy.xpath(ScheduleTasksLocators.ScheduleTasks.scheduleTable)
        .should('contains.text','Synchronization (PayPal Zettle plugin)').each(($row,index,$rows) =>{
            cy.log($row.text())
        })}
    //"Verify Schedule Tasks Tab Data"
    verifyScheduleTab(){cy.get(ScheduleTasksLocators.ScheduleTasks.shedultTabData)
        .eq(2).within(function(){
            cy.get('td').eq(7).should('contains.text','Run now')
    })}
    //"Edit Button "
    clickEditButton(){cy.xpath(ScheduleTasksLocators.ScheduleTasks.editButton).click()}
    //"Enter Seconds Run Periood"
    entSecoudsRunPred(SRP){cy.xpath(ScheduleTasksLocators.ScheduleTasks.secondsRunPrd).clear().type(SRP)}
    //"Update Button"
    clickUpdateButton(){cy.xpath(ScheduleTasksLocators.ScheduleTasks.updateBtn).click()
        cy.on('window:alert',function(alert){
            expect(alert).eq('For security purposes, editing tasks is not allowed on the demo site.')
        })}
    //"Schedule Tasks link"
    clickScheduleLink(){cy.xpath(ScheduleTasksLocators.ScheduleTasks.scheduleTasksLink).invoke('removeAttr','target').click()}
    //"Verify Schedule Tasks Page Title"
    verifySchedultTasksPageTitle(){cy.xpath(ScheduleTasksLocators.ScheduleTasks.scheduleTasksPageTitle)
        .should('have.text','Schedule tasks').go('back')}
    //"Verify Schedule Tasks Main Page title"
    verifyScheduleTasksMainPageTitle(){cy.get(ScheduleTasksLocators.ScheduleTasks.scheduleTasksMainPageTitle)
        .should('have.text','\nSchedule tasks\n')}
        

}