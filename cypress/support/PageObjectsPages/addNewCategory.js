const addNewCategoryLocators = require('../PagesElements/addNewCategoryLocators.json')
    let brokenLinks = 0
    let activeLinks = 0
   class AddNewCategory
{
    catalog= "//p[normalize-space()='Catalog']"
    catgor = "//p[normalize-space()='Categories']"
    addNew = "//a[normalize-space()='Add new']"
    catName= "//input[@id='Name']"
    description = "#Description_ifr"
    patcat= "//select[@id='ParentCategoryId']"
    save= "button[name='save']"

    //"Menu Calalog" click option
    clickCatalog() {cy.xpath(this.catalog).click({force: true})}   
    //"Bar Categories" click option
    clickCategories(){cy.xpath(this.catgor).click({force: true})}                
    //"Add New" click option
    clickAddNew(){cy.xpath(this.addNew).click()}            
    //"Input Name" enter Name
    entName(NM){cy.xpath(this.catName).type(NM)}    
    //"input Description" Input option 
    entDescription(Des){cy.getIframe(addNewCategoryLocators.AddNewCategory.description)
        .type(Des, {delay: 0}).should('be.visible')}
    //"Select Category" select option
    selPaCategory() {
        cy.xpath(addNewCategoryLocators.AddNewCategory.patcat).select('Computers').should('have.value','1')
        return
    }
    //"Click Save" Save option 
    clickSave(){
        cy.get(addNewCategoryLocators.AddNewCategory.save).should('have.css','background-color','rgb(60, 141, 188)').click()
        return
    }
    //"Categories Link "
    clickCateLink(){cy.xpath(addNewCategoryLocators.AddNewCategory.categoriesLink)
        .invoke('removeAttr','target').click()}
    //"Verify Categories page title"
    verifyCatePageTitle(){cy.get(addNewCategoryLocators.AddNewCategory.categoriesPageTitle)
        .should('contains.text','Categories')
        cy.get('a').each(($link, index)=>{
            const href = $link.attr('href')
            if(href){
                cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`*****link ${index +1} is Broken link *****${href}`)
                        brokenLinks++
                    }else
                    {
                        cy.log(`***** link ${index + 1} Active link*****`)
                        activeLinks++
                    }
                })
            }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
    }).go('back')
    return
}
    //"Verify Categories Main page title"
    verifyCateMainPageTitle(){cy.xpath(addNewCategoryLocators.AddNewCategory.categoriesMainPageTitle)
        .should('be.visible')}

    
}
export default AddNewCategory