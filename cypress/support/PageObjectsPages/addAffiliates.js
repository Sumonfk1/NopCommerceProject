//let { generateRandomData } = require('../support/faker');
//const addAffiliatesLocators = require('../PagesElements/addAffiliatesLocators.json')
class AddAffiliates
{

    promotions="//p[normalize-space()='Promotions']"
    affiliates="//p[normalize-space()='Affiliates']"
    addNew="//a[normalize-space()='Add new']"
    fName="//input[@id='Address_FirstName']"
    lName="//input[@id='Address_LastName']"



    
    //"Menu Promotions" click option
    clickPromotions(){cy.xpath(this.promotions).click({force: true})}
    //"Bar Discount" click option
    clickAffiliates(){cy.xpath(this.affiliates).click({force: true})}
    //"Add New" click option
    clickAddNew(){cy.xpath(this.addNew).click()}
    //"input FirstName" input option
    entFirstName(fn){cy.xpath(this.fName).type(fn)}
    //"Input LastName" input option 
    entLastName(ln){cy.xpath(this.lName).type(ln)}
        
}


export default AddAffiliates
