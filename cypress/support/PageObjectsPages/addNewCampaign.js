
class AddNewCampaign
{
    //Element for AddNewCampaign
    
    promotions="//p[normalize-space()='Promotions']"
    campaign="//p[normalize-space()='Campaigns']"
    addNew="//a[normalize-space()='Add new']"
    name="//input[@id='Name']"
    subject="//input[@id='Subject']"
    body="//textarea[@id='Body']"
    plannedDate="//input[@id='DontSendBeforeDate']"
    limitedStore="//select[@id='StoreId']"
    limitedCustomer="//select[@id='CustomerRoleId']"
    save="//button[@name='save']"
    emailCampaigns="//a[normalize-space()='email campaigns']"
    emailCampTitle="#email-campaigns"
    mainPageTitle="//h1[normalize-space()='Campaigns']"
    editBtn ="(//a[normalize-space()='Edit'])[1]"
    eName="//input[@id='Name']"
    customerRole="//select[@id='CustomerRoleId']"
    editsave="button[name='save']"
    successfulMessage=".alert.alert-success.alert-dismissable"


    //Action methos for AddNewCampaign

    //"Menu Promotions" click option
    clickPromotions(){cy.xpath(this.promotions).click({force: true})}
    //"Bar Campaigns" click option 
    clickCampaigns(){cy.xpath(this.campaign).click({force:true})}
    //"Add New" click option
    clickAddNew(){cy.xpath(this.addNew).click()}
    //"Input Name" Input option
    entName(nm){cy.xpath(this.name).type(nm)}
    //"Input Subject" input option 
    entSubject(sb){cy.xpath(this.subject).type(sb)}
    //"input Body" input option 
    entBody(b){cy.xpath(this.body).type(b,{delay:0})
        .type('{selectall}')}                                      
    //"input Date" input option 
    entPlanDate(pd){cy.xpath(this.plannedDate).type(pd)}
    //"Select Limitied to Store" select option 
    selLimitedStore(){cy.xpath(this.limitedStore)
            .select('Your store name').should('have.value','1')}
    //"Select Limited to Csutomer role" Selct option 
    selLimitedCustomer(){cy.xpath(this.limitedCustomer)
        .select('Forum Moderators').should('have.value','2')}
    //"click save" click option 
    clicksave(){cy.xpath(this.save).click()
        .should('be.visible','The new campaign has been added successfully.')}
    //"Verify Email Campaings page"
    verifyEmailCampaings(){cy.xpath(this.emailCampaigns)
        .invoke('removeAttr','target').click()}                                                      
    //"Verify Email Campaings title"
    verifyEmailCampTitle(){cy.get(this.emailCampTitle)
        .should('contains.text','Email campaigns').go('back')}
    //"Verify Main Page Email Campaings title"
    verifyMainEmailPageTitle(){cy.xpath(this.mainPageTitle)
        .should('contains.text','Campaigns')}
    //"Edit Button"
    clickEdit(){cy.xpath(this.editBtn).click({force: true})}
    //"Edit Compaigns Name"
    editCamName(EN){cy.xpath(this.eName).clear().type(EN,{delay:0})}
    //"Select Customer Role"
    selCustomerRole(){cy.xpath(this.customerRole)
        .select('Administrators').should('be.visible')}
    //"Click Save"
    clickeditSave(){cy.get(this.editsave).click({force: true})}
    //"Verify Successful message"
    verifySuccessfulMegs(){cy.get(this.successfulMessage)
        .should('contain.text','The campaign has been updated successfully.')}
}
export default AddNewCampaign