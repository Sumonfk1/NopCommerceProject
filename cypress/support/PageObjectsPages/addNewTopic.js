const addNewTopicsLocators = require('../PagesElements/addNewTopics.json')
export class AddNewTopic
{

    // "Menu Content management" click option 
    clickContentManagement(){cy.xpath(addNewTopicsLocators.AddNewTopics.contentManag).click({force: true})
        return
    }
    //"Bar Topics" click option 
    clickTopics(){cy.xpath(addNewTopicsLocators.AddNewTopics.topic).click({force: true})
        return    
    }
    //"Add New" click option 
    clickAddNew(){cy.xpath(addNewTopicsLocators.AddNewTopics.addNew).click()
        return
    }
    //"Enter Title " Input option 
    entTitle(til){cy.xpath(addNewTopicsLocators.AddNewTopics.title).type(til)
        return
    }
    //"Enter Body" Input option
    entBody(bdy){cy.xpath(addNewTopicsLocators.AddNewTopics.body).then(function($iframe){
        let ifraamebody = $iframe.contents().find('body')
            cy.wrap(ifraamebody).type(bdy,{delay:0}).type('{selectall}')
            return
        })}

    //"Verify Topics Table Data"
    VerifyTopicTable(){cy.xpath(addNewTopicsLocators.AddNewTopics.topicsTable)
        .should('contains.text',"CheckoutAsGuestOrRegister").and('have.length','12')
        .each(($row,index,$rows)=>{
            cy.log($row.text())
            return
        })}

    //"Click Save"
    clickSave(){cy.xpath(addNewTopicsLocators.AddNewTopics.save).click()
        return
    }
    //"Back to topic list"
    clickBackList(){cy.xpath(addNewTopicsLocators.AddNewTopics.backlist).click()
        return
    }
        
        


}

//export default AddNewTopic