class CheckoutAttributes
{
    catalog = "//p[normalize-space()='Catalog']"
    attributes = "//p[normalize-space()='Attributes']"
    checkoutAttrib = "//p[normalize-space()='Checkout attributes']"
    addNew = "//a[normalize-space()='Add new']"
    onOffSwitch = "//span[@class='onoffswitch-inner']"
    attributesInfo = "#checkout-attributes-info"
    nameChckAtt = "#Name"
    controlType = "//select[@id='AttributeControlTypeId']"
    required = "//input[@id='IsRequired']"
    checkOutDisOrder = "//input[@title='0 ']"
    save = '[name="save"]'



     //"Menu Calalog" click option     
     clickCatalog(){cy.xpath(this.catalog).click({force: true})}
     //"Bar ManuFactures" click option 
     clickAttributes(){cy.xpath(this.attributes).click({force: true})}
     //"Bar MenuFactures"
     clickCheckoutAttri(){cy.xpath(this.checkoutAttrib).click({force: true})}
     //"Add New Checkout Attribute"
     clickAddNew(){cy.xpath(this.addNew).click()}
     //"OnOffSwitch"
     clikcOnOf(){cy.xpath(this.onOffSwitch).click({force: true})}
     //"Attributes Info Checkout Attribute"
     clickAttributeInfo(){cy.get(this.attributesInfo).click()}
     //" Checkout Attribute Name"
     entCkAtName(Can){cy.get(this.nameChckAtt).type(Can)}
     //"Select Control Type"
     selControlType(){cy.xpath(this.controlType).select('File upload').and('be.visible')}
     //" Required Check box"
     checkRequired(){cy.xpath(this.required).click()}
     //"Checkout Attribute Display order"
     entCheckAttDisOrd(DO){cy.xpath(this.checkOutDisOrder).type(DO)}
     //"Save button"
     clickSave(){cy.get(this.save).click()}
     
}

export default CheckoutAttributes