class SpecificationAttributes
{
    catalog = "//p[normalize-space()='Catalog']"
    attributes = "//p[normalize-space()='Attributes']"
    specficaAttb = "//p[normalize-space()='Specification attributes']"
    addGroup = "//a[normalize-space()='Add group']"
    name = "//input[@id='Name']"
    displayOrder = "//input[@title='0 ']"
    save = "//button[@name='save']"
    successMesg = "//div[@class='alert alert-success alert-dismissable']"
    tableData = "//table[@id='specificationattributegroups-grid']//tbody//tr"
    


    //"Menu Calalog" click option     
    clickCatalog(){cy.xpath(this.catalog).click({force: true})}
    //"Bar ManuFactures" click option 
    clickAttributes(){cy.xpath(this.attributes).click({force: true})}
    //"Bar Meanufactures" click option
    clickSpecifiAttb(){cy.xpath(this.specficaAttb).click({force: true})}
    //"Add Group Button"
    clickAddGroup(){cy.xpath(this.addGroup).click()}
    //"Enter Attributed Group Name" input option 
    entName(en){cy.xpath(this.name).type(en)}
    //"Enter Display Order number" input option
    entDisOrd(Dso){cy.xpath(this.displayOrder).type(Dso)}
    //"Save button "
    clickSave(){cy.xpath(this.save).click()}
    //"Verify Successfully Message text"
    VerifySuccessMesg(){cy.xpath(this.successMesg).should('contains.text','The new attribute group has been added successfully.')}
    //"Verify Table data"
    VerifyTableData(){cy.xpath(this.tableData).should('contains.text','6')
        .each(($row, index, $rows) =>{
            cy.xpath("//select[@name='specificationattributegroups-grid_length']").select('100')
            cy.log($row.text())
        })}
   

}

export default SpecificationAttributes