class Customers
{
    //Elements for Customers
    manuCust="//a[@href='#']//p[contains(text(),'Customers')]"
    barCust="//a[@href='/Admin/Customer/List']//p[contains(text(),'Customers')]"
    addNew="//a[normalize-space()='Add new']"
    aEmail="//input[@id='Email']"
    sPw="//input[@id='Password']"
    SFname="//input[@id='FirstName']"
    SLname="//input[@id='LastName']"
    Gender="//input[@id='Gender_Male']"
    DoB="//input[@id='DateOfBirth']"
    save="//button[@name='save']"
    customersList = "//table[@id='customers-grid']//tbody//tr"


    
    //"manuCustomers" click options
     clickCustomers(){cy.xpath(this.manuCust).click({force: true})}
     //"barCustomers" click options         
     clickBarCustomers(){cy.xpath(this.barCust).click({force: true})}
     //"Add New" Click Options 
     clickAddNew(){cy.xpath(this.addNew).click()}
     //"input email" enter email           
     entEmail(EM){cy.xpath(this.aEmail).type(EM)}
     //"input Password" enter Password
     entPassword(PW){cy.xpath(this.sPw).type(PW)}
     //"Input FirstName" enter first name 
     entFirstName(SFN){cy.xpath(this.SFname).type(SFN)}
     //"Input LastName" enter last name
     entLastName(SLN){cy.xpath(this.SLname).type(SLN)}
     //"Select Gender" select opetions
     selGend(){cy.xpath(this.Gender).click()}
     //"Select Date of Birth" select DOB
     selDob(DB){cy.xpath(this.DoB).type(DB)}
     //"Select Save button"
     selSave(){cy.xpath(this.save).click()}
     //"Verify Customer list"
     VerifyCustomerList(){cy.xpath(this.customersList)
          .should('contains.text','Mohammed Uddin')
               .each(($row, index, $rows) =>{
                    cy.log($row.text())
          })}
                    
            

}

export default Customers