const CustomerByOrderLocators = require('../PagesElements/addCustomersByOrder.json')
    let brokenLinks = 0
    let activeLinks = 0
export class CustomerByOrderTotal
{
    //"Menu Customer Tab"
    clickCustomer(){cy.xpath(CustomerByOrderLocators.customerByOrder.customerReports).click({force: true})}
    //"Customers By Order total"
    clickCustomerOrder(){cy.xpath(CustomerByOrderLocators.customerByOrder.customerOrderTotal).click({force: true})}
    //"Verify Customer Table Data"
    verifyCustomerTable(){cy.get(CustomerByOrderLocators.customerByOrder.customerTable)
        .should('contains.text','arthur_holmes@nopCommerce.com').each(($row, index, $rows)=>{
            cy.log($row.text())
        })}
    //"Customer Report Link"
    clickCustomerReportsLink(){cy.xpath(CustomerByOrderLocators.customerByOrder.customerReportLink)
        .invoke('removeAttr','target').click()}
    //"Verify Customer Page Title"
    verifyCustomerPageTitle(){cy.xpath(CustomerByOrderLocators.customerByOrder.customerPageTitle)
        .should('contains.text','Reports')
        cy.get('a').each(($link,index)=>{
            const href = $link.attr('href')
            if(href){
                cy.request({url: href, failOnStatusCode: false}).then((response)=>{
                    if(response.status>=400){
                        cy.log(`*****link ${index +1} is Broken Link *****${href}`)
                        brokenLinks++
                    }else
                    {
                        cy.log(`***** link ${index + 1} Active Link *****`)
                        activeLinks++
                    }
                })
            }
        }).then(($links)=>{
            const totalLinks = $links.length
            cy.log(`*****total links**** ${totalLinks}` )
            cy.log(`**** broken links ***** ${brokenLinks}`)
            cy.log(`***** active links***** ${activeLinks}`)
        }).go('back')}
    //"Verify Customer Main Page Title"
    verifyCustomerMainPageTitle(){cy.xpath(CustomerByOrderLocators.customerByOrder.customerMainPageTitle)
        .should('contains.text','Customers by order total')}
    
    
    }





 