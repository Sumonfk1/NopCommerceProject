// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//import 'cypress-file-upload';
/// 

  /// <reference types="cypress-xpath" />

 /// require('@4tw/cypress-drag-drop')
/// import 'cypress-file-upload';
///<reference types="Cypress"/>,
///<reference types="cypress-xpath" />,
///require('@4tw/cypress-drag-drop')

Cypress.Commands.add("login",(emailId,password) =>{
    

    cy.fixture('nopcommerce').then((data)=>{
    
            cy.visit(Cypress.config('qaurl'))
            cy.xpath("//*[@id='Email']").clear().type(emailId)
            cy.xpath("//*[@id='Password']").clear().type(password)
            cy.xpath("//button[@type='submit']").click()
            cy.intercept({resourceType: /xhr|fetch/},{log: false})
            cy.title().should('be.equal','Dashboard / nopCommerce administration')
            //var randomEmail = Math.random().toString(36).substring(2,15)+"@gmail.com"              
        })

Cypress.Commands.add("logout",()=>{
    cy.xpath("//a[normalize-space()='Logout']").click()
    cy.xpath("//strong[normalize-space()='Welcome, please sign in!']").should('be.text','Welcome, please sign in!')
})

})

Cypress.Commands.add('getIframe', (iframe)=>{
    cy.get(iframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then(cy.wrap)
})

Cypress.Commands.add('DatePicker',(date)=>{
    cy.get(date)
    cy.get("span[aria-controls='StartDate_dateview']").click()
    cy.get('.k-calendar-view tbody tr td').each(($el, index, $list)=>{
        var month = $el.text()
        if(month == 'April')
        {
            cy.wrap($el).click()
        }
    })

})